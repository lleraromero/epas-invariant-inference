# README #

### What is this directory for? ###

Here is the ListItr C# solution. i.e. you will find here the ListItr project and files for working with VisualStudio.

You will find the ListItr project here. Where there are mainly two files:

* ListItr.cs is the C# ListItr class used for generating the EPA.

* ListItr_NC.cs is the code of ListItr.cs performed for generating the conditions that will be used as splitting conditions using the static CodeContracts checker.