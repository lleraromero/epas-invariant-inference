﻿using System;
using System.Diagnostics.Contracts;
using ConcurrentModificationException = System.Exception;
using IllegalStateException = System.Exception;
using NoSuchElementException = System.Exception;

namespace Examples.ICSE2011
{
    /// <summary>
    /// Java 1.6 implementation of ArrayList and ListItr
    /// </summary>
    public class ArrayListJava
    {
        // AbstractList
        public int modCount = 0;
        // AbtrasctList

        public Object[] elementData;
        public int size;

        //public ArrayListJava(int initialCapacity)
        //{
        //    if (initialCapacity < 0)
        //        throw new ArgumentException("Illegal Capacity: " + initialCapacity);
        //    this.elementData = new Object[initialCapacity];
        //}

        public ArrayListJava()
        {
            this.elementData = new Object[10];
            this.size = 0;
        }

        Object get(int index)
        {
            rangeCheck(index);

            return (Object)elementData[index];
        }

        public Object remove(int index)
        {
            rangeCheck(index);

            modCount++;
            Object oldValue = elementData[index];

            int numMoved = size - index - 1;
            if (numMoved > 0)
                Array.Copy(elementData, index + 1, elementData, index, numMoved);
            elementData[--size] = null; // Let gc do its work

            return oldValue;
        }

        public Object set(int index, Object element)
        {
            rangeCheck(index);

            Object oldValue = elementData[index];
            elementData[index] = element;
            return oldValue;
        }

        void ensureCapacity(int minCapacity)
        {
            modCount++;
            int oldCapacity = elementData.Length;
            if (minCapacity > oldCapacity)
            {
                Object[] oldData = elementData;
                int newCapacity = (oldCapacity * 3) / 2 + 1;
                if (newCapacity < minCapacity)
                    newCapacity = minCapacity;
                // minCapacity is usually close to size, so this is a win:
                Array.Resize(ref elementData, newCapacity);
                Contract.Assume(elementData.Length == newCapacity);
            }
        }

        public void add(int index, Object element)
        {
            rangeCheckForAdd(index);

            ensureCapacity(size + 1); // Increments modCount!!
            Array.Copy(elementData, index, elementData, index + 1, size - index);
            elementData[index] = element;
            size++;
        }

        private void rangeCheck(int index)
        {
            if (index >= size)
                throw new IndexOutOfRangeException("Index: " + index + ", Size: " + size);
        }

        private void rangeCheckForAdd(int index)
        {
            if (index > size || index < 0)
                throw new IndexOutOfRangeException("Index: " + index + ", Size: " + size);
        }
    }

    /*
    The preconditions were extracted from ListItr.c
    */
    // TODO: add support for nested classes
    public class ListItr
    {
        /* Itr */
        public int cursor; // index of next element to return
        public int lastRet; // index of last element returned; -1 if no such
        public int expectedModCount;
        /* Itr */

        public ArrayListJava array;

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            //GUIDO'S INVARIANT
            //Contract.Invariant(lastRet == -1 || cursor - 1 <= lastRet);
            //Contract.Invariant(lastRet <= cursor);
            
            //DAIKON'S INVARIANT (WH)
            /*
            //--Contract.Invariant(array.size == array.size);
            Contract.Invariant(this.cursor >= 0);
            Contract.Invariant(this.lastRet >= -1);
            Contract.Invariant(this.expectedModCount >= 0);
            Contract.Invariant(array != null);
            //--Contract.Invariant(Contract.ForAll(array, x => x != null));
            Contract.Invariant(array.elementData != null);
            //--Contract.Invariant(array.elementData.GetType() == typeof(Object[]));
            //--Contract.Invariant(array.elementData.Length.OneOf(10, 16, 133));
            Contract.Invariant(this.cursor >= this.lastRet);
            //Contract.Invariant(this.cursor <= this.expectedModCount);
            //--Contract.Invariant(this.cursor < array.serialVersionUID);
            Contract.Invariant(this.cursor <= array.size);
            Contract.Invariant(this.cursor <= array.elementData.Length);
            //Contract.Invariant(this.lastRet < this.expectedModCount);
            //--Contract.Invariant(this.lastRet < array.serialVersionUID);
            Contract.Invariant(this.lastRet <= array.size - 1);
            Contract.Invariant(this.lastRet <= array.elementData.Length - 1);
            //--Contract.Invariant(this.expectedModCount < array.serialVersionUID);
            //Contract.Invariant(this.expectedModCount >= array.size);
            //--Contract.Invariant(array.serialVersionUID > array.size);
            //--Contract.Invariant(array.serialVersionUID > array.elementData.Count());
            Contract.Invariant(array.size <= array.elementData.Length);
            Contract.Invariant(array.size - 1 <= array.elementData.Length - 1);
            */

            /*
            //C1********************************************************************
            //--Contract.Invariant(array.size == array.size);
            Contract.Invariant(this.cursor >= 0);
            Contract.Invariant(this.lastRet >= -1);
            Contract.Invariant(this.expectedModCount >= 0);
            Contract.Invariant(array != null);
            //--Contract.ForAll(ar.uba.dc.listitr.java.util.ArrayList.this, x => x != null)
            Contract.Invariant(array.elementData != null);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.this.elementData.GetType() == typeof(java.lang.Object[]));
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.this.elementData.Count().OneOf(10, 16, 133));
            Contract.Invariant(this.cursor >= this.lastRet);
            //Contract.Invariant(this.cursor <= this.expectedModCount);
            //--Contract.Invariant(this.cursor < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            Contract.Invariant(this.cursor <= array.size);
            Contract.Invariant(this.cursor <= array.elementData.Length);
            Contract.Invariant(this.cursor != array.elementData.Length-1);
            //Contract.Invariant(this.lastRet < this.expectedModCount);
            //--Contract.Invariant(this.lastRet < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            Contract.Invariant(this.lastRet <= array.size-1);
            Contract.Invariant(this.lastRet < array.elementData.Length-1);
            //--Contract.Invariant( this.expectedModCount < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            //Contract.Invariant(this.expectedModCount >= array.size);
            Contract.Invariant(this.expectedModCount != array.elementData.Length-1);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID > ar.uba.dc.listitr.java.util.ArrayList.this.size);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID > ar.uba.dc.listitr.java.util.ArrayList.this.elementData.Count());
            Contract.Invariant(array.size <= array.elementData.Length);
            Contract.Invariant(array.size-1 <= array.elementData.Length-1);
            //**************************************************************************
            */

            /*
            //C2********************************************************************
            //--Contract.Invariant(array.size == array.Count());
            Contract.Invariant(this.cursor >= 0);
            Contract.Invariant(this.lastRet >= -1);
            Contract.Invariant(this.expectedModCount >= 0);
            Contract.Invariant(array != null);
            //--Contract.ForAll(ar.uba.dc.listitr.java.util.ArrayList.this, x => x != null)
            Contract.Invariant(array.elementData != null);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.this.elementData.GetType() == typeof(java.lang.Object[]));
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.this.elementData.Count().OneOf(10, 16, 133));
            Contract.Invariant(this.cursor >= this.lastRet);
            //Contract.Invariant(this.cursor <= this.expectedModCount);
            //--Contract.Invariant(this.cursor < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            Contract.Invariant(this.cursor <= array.size);
            Contract.Invariant(this.cursor <= array.elementData.Length);
            Contract.Invariant(this.cursor != array.elementData.Length-1);
            //Contract.Invariant(this.lastRet < this.expectedModCount);
            //--Contract.Invariant(this.lastRet < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            Contract.Invariant(this.lastRet <= array.size-1);
            Contract.Invariant(this.lastRet < array.elementData.Length-1);
            //--Contract.Invariant(this.expectedModCount < ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID);
            //Contract.Invariant(this.expectedModCount >= array.size);
            Contract.Invariant(this.expectedModCount != array.elementData.Length-1);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID > ar.uba.dc.listitr.java.util.ArrayList.this.size);
            //--Contract.Invariant(ar.uba.dc.listitr.java.util.ArrayList.serialVersionUID > ar.uba.dc.listitr.java.util.ArrayList.this.elementData.Count());
            Contract.Invariant(array.size <= array.elementData.Length);
            Contract.Invariant(array.size-1 <= array.elementData.Length-1);
            //===========================================================================
            //ar.uba.dc.listitr.java.util.ArrayList$ListItr:::OBJECT;condition="not(0 > (this.lastRet - 1))"
            Contract.Invariant( (0 > (this.lastRet - 1) ) || (
                this.expectedModCount == array.size &&
                //this.expectedModCount == ar.uba.dc.listitr.java.util.ArrayList.this.Count()
                //ar.uba.dc.listitr.java.util.ArrayList.this[this.cursor-1] == ar.uba.dc.listitr.java.util.ArrayList.this.elementData[this.cursor-1]
                //ar.uba.dc.listitr.java.util.ArrayList.this[this.expectedModCount-1] == ar.uba.dc.listitr.java.util.ArrayList.this.elementData[this.expectedModCount-1]
                //ar.uba.dc.listitr.java.util.ArrayList.this[this.expectedModCount-1] == ar.uba.dc.listitr.java.util.ArrayList.this.elementData[ar.uba.dc.listitr.java.util.ArrayList.this.size-1]
                this.cursor >= 1 &&
                this.lastRet != 0 &&
                this.expectedModCount >= 2 &&
                //--ar.uba.dc.listitr.java.util.ArrayList.this.elementData.Count().OneOf(10, 133)
                this.cursor < array.elementData.Length-1 &&
                this.expectedModCount <= array.elementData.Length )
            );
            //**************************************************************************
            */
              
            //C3********************************************************************
            //equals to C1
            //**************************************************************************

            //C4********************************************************************
            //equals to C1
            //**************************************************************************
        }


        public ListItr(ArrayListJava a, int index)
        {
            Contract.Requires(a != null);
            Contract.Requires(a.elementData != null);
            
            Contract.Requires(0 <= index && index <= a.size);
            Contract.Requires(0 <= a.size && a.size <= a.elementData.Length);
            Contract.Requires(0 <= a.modCount);
            Contract.Requires(10 <= a.elementData.Length);
            
            array = a;
            array.size = a.size;

            cursor = index;
            lastRet = -1;
            expectedModCount = a.modCount;
        }

        /* Itr */
        private void checkForComodification()
        {
            if (array.modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }

        public bool hasNext()
        {
            return cursor != array.size;
        }

        // DIFF with Java 1.4
        public Object next()
        {
            Contract.Requires(array.elementData != null);
            Contract.Requires(array.modCount == expectedModCount);
            Contract.Requires(0 <= cursor && cursor < array.size);

            checkForComodification();
            int i = cursor;
            if (i >= array.size)
            {
                throw new NoSuchElementException();
            }
            Object[] elementData = array.elementData;
            if (i >= elementData.Length)
            {
                throw new ConcurrentModificationException();
            }
            cursor = i + 1;
            return (Object)elementData[lastRet = i];
        }

        // DIFF with Java 1.4
        public void remove()
        {
            Contract.Requires(array.elementData != null);
            Contract.Requires(lastRet != -1);
            Contract.Requires(array.modCount == expectedModCount);
            Contract.Requires(0 <= lastRet && lastRet < array.size);

            if (lastRet < 0)
            {
                throw new IllegalStateException();
            }
            checkForComodification();

            try
            {
                array.remove(lastRet);
                cursor = lastRet;
                lastRet = -1;
                expectedModCount = array.modCount;
            }
            catch (IndexOutOfRangeException)
            {
                throw new ConcurrentModificationException();
            }
        }
        /* Itr */

        public bool hasPrevious()
        {
            return cursor != 0;
        }

        // DIFF with Java 1.4
        public object previous()
        {
            Contract.Requires(array.elementData != null);
            Contract.Requires(array.modCount == expectedModCount);
            Contract.Requires(0 <= cursor - 1 && cursor - 1 < array.size);

            checkForComodification();
            int i = cursor - 1;
            if (i < 0)
            {
                throw new NoSuchElementException();
            }
            Object[] elementData = array.elementData;
            if (i >= elementData.Length)
            {
                throw new ConcurrentModificationException();
            }
            cursor = i;
            return (Object)elementData[lastRet = i];
        }

        public int nextIndex()
        {
            return cursor;
        }

        public int previousIndex()
        {
            return cursor - 1;
        }

        // DIFF with Java 1.4
        public void set(Object e)
        {
            Contract.Requires(array.elementData != null);
            Contract.Requires(lastRet != -1);
            Contract.Requires(array.modCount == expectedModCount);
            Contract.Requires(0 <= lastRet && lastRet < array.size);

            if (lastRet < 0)
            {
                throw new IllegalStateException();
            }
            checkForComodification();

            try
            {
                array.set(lastRet, e);
            }
            catch (IndexOutOfRangeException)
            {
                throw new ConcurrentModificationException();
            }
        }

        public void add(Object e)
        {
            Contract.Requires(array.elementData != null);
            Contract.Requires(array.modCount == expectedModCount);
            Contract.Requires(0 <= cursor && cursor <= array.size);
            
            checkForComodification();

            try
            {
                int i = cursor;
                array.add(i, e);
                cursor = i + 1;
                lastRet = -1;
                expectedModCount = array.modCount;
            }
            catch (IndexOutOfRangeException)
            {
                throw new ConcurrentModificationException();
            }
        }
    }
}