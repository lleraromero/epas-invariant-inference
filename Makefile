## Typical usage:
# make classes
# make randoop
# make test-classes
# make splitinfo
# make dyncomp
# make chicory
# or: make chicory-with-dyncomp
# make daikon
# make printinv
# or: make printenv-all

DAIKON_CLASSPATH=$(DAIKONDIR)/java:$(pl)/java/src:$(DAIKONDIR)/daikon.jar

# DAIKON_EXTRA_ARGS="--config_option daikon.split.PptSplitter.suppressSplitterErrors=false --config_option daikon.split.SplitterFactory.delete_splitters_on_exit=false"

all: classes randoop test-classes splitinfo dyncomp chicory daikon printinv

# Compile the target program
classes:
	mkdir -p target/classes
	javac -g -d target/classes/ src/main/java/ar/uba/dc/listitr/java/util/*.java

classes/ar/uba/dc/listitr/Runner.class: src/main/java/ar/uba/dc/listitr/Runner.java
	javac -g -d target/classes/ -cp lib/randoop/randoop-1.3.6-pre.jar $<

# Run Randoop on the target program.
# Output appears in src/test/java/ar/uba/dc/listitr/test/ .
randoop: classes/ar/uba/dc/listitr/Runner.class
	## Command-line arguments go in file src/main/java/ar/uba/dc/listitr/Runner.java .
	java -cp "lib/randoop/randoop-1.3.6-pre.jar:target/classes/" ar.uba.dc.listitr.Runner

# Compile the Randoop-generated tests
test-classes:
	mkdir -p target/test-classes
	javac -g -d target/test-classes -cp "target/classes:lib/junit/junit-4.12.jar" src/test/java/ar/uba/dc/listitr/test/*.java

# Run the tests
run-tests:
# This needs JUnit, and Daikon is a convenient place to get JUnit from.
	java -cp "$(DAIKON_CLASSPATH):target/classes:target/test-classes" ar.uba.dc.listitr.test.ListItrTest

# Add this command-line argument? --nesting-depth=8
dyncomp: ListItrTest.decls-DynComp
ListItrTest.decls-DynComp: target/test-classes/ar/uba/dc/listitr/test/ListItrTest.class
	java -cp "$(DAIKON_CLASSPATH):target/classes:target/test-classes" daikon.DynComp ar.uba.dc.listitr.test.ListItrTest

# ListItrTest.decls: ListItrTest.decls-DynComp
# 	cp -pf $< $@

# splitinfo: splitinfo-full
splitinfo: splitinfo-C4

splitinfo-full:
	java -cp "$(DAIKON_CLASSPATH)" daikon.tools.jtb.CreateSpinfo -o ArrayList.spinfo src/main/java/ar/uba/dc/listitr/java/util/ArrayList.java

splitinfo-1:
	\cp -f ArrayList.spinfo_lastRet_lessthan_0 ArrayList.spinfo

splitinfo-2:
	\cp -f ArrayList.spinfo_lastRet_equals_-1 ArrayList.spinfo

splitinfo-3:
	\cp -f ArrayList.spinfo_cursor-1_lessequal_lastRet ArrayList.spinfo

splitinfo-NC:
	\cp -f ArrayList.spinfo_NC ArrayList.spinfo

splitinfo-WP:
	\cp -f ArrayList.spinfo_WP ArrayList.spinfo

splitinfo-C2:
	\cp -f ArrayList.spinfo_C2 ArrayList.spinfo

# C1, C3, and C4 use "this.array.size", but ListItr has no field named array.
# Did it previously?
splitinfo-C1:
	\cp -f ArrayList.spinfo_C1 ArrayList.spinfo
splitinfo-C3:
	\cp -f ArrayList.spinfo_C3 ArrayList.spinfo
splitinfo-C4:
	\cp -f ArrayList.spinfo_C4 ArrayList.spinfo

chicory: chicory-without-dyncomp

chicory-without-dyncomp:
	java -cp "$(DAIKON_CLASSPATH):target/classes:target/test-classes" daikon.Chicory ar.uba.dc.listitr.test.ListItrTest

chicory-with-dyncomp:
	java -cp "$(DAIKON_CLASSPATH):target/classes:target/test-classes" daikon.Chicory --comparability-file=ListItrTest.decls-DynComp ar.uba.dc.listitr.test.ListItrTest

daikon:
	java -cp "$(DAIKON_CLASSPATH)" daikon.Daikon --config_option daikon.split.SplitterFactory.compiler="javac -source 1.7 -cp $(DAIKON_CLASSPATH)" --ppt-select-pattern=".*ArrayList." ListItrTest.dtrace.gz ArrayList.spinfo $(DAIKON_EXTRA_ARGS)

daikon-one-file:
	java -cp "$(DAIKON_CLASSPATH)" daikon.Daikon --config_option daikon.split.SplitterFactory.compiler="javac -source 1.7 -cp $(DAIKON_CLASSPATH)" --ppt-select-pattern=".*ArrayList.ListItr.add|ArrayList.ListItr:::OBJECT" ListItrTest.dtrace.gz ArrayList.spinfo

printinv: printinv-object-invariant

# Only prints the object invariant
printinv-object-invariant:
	java -cp "$(DAIKON_CLASSPATH)" daikon.PrintInvariants --format CSharpContract --ppt-select-pattern ".*:::OBJECT" ListItrTest.inv.gz

printinv-all:
	java -cp "$(DAIKON_CLASSPATH)" daikon.PrintInvariants --format CSharpContract ListItrTest.inv.gz

clean:
	rm -rf target
	rm -rf src/test
