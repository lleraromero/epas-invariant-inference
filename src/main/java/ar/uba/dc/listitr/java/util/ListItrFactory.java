package ar.uba.dc.listitr.java.util;

import ar.uba.dc.listitr.java.util.ArrayList.ListItr;

public class ListItrFactory {

    /**
     * @return iterador sobre un array vacio
     */
    public static ListItr newInstance1() {
        ArrayList arrayList = new ArrayList();
        return (ListItr) arrayList.listIterator();
    }

    /**
     * @return iterador al comienzo del array
     */
    public static ListItr newInstance2(int size) {
        ArrayList arrayList = createNonEmptyArray(size);
        return (ListItr) arrayList.listIterator();
    }

    /**
     * @return iterador al final del array
     */
    public static ListItr newInstance3(int size) {
        ArrayList arrayList = createNonEmptyArray(size);
        return (ListItr) arrayList.listIterator(size);
    }

    /**
     * @return iterador en el medio del array
     */
    public static ListItr newInstance4(int size, int index) {
        if (index < 1)
            throw new IndexOutOfBoundsException();

        ArrayList arrayList = createNonEmptyArray(size);

        return (ListItr) arrayList.listIterator(index % size);
    }

    private static ArrayList createNonEmptyArray(int size) {
        if (size < 1)
            throw new IndexOutOfBoundsException();

        ArrayList arrayList = new ArrayList();

        for (int i = 0; i < size; i++)
        {
            arrayList.add(new Object());
        }
        return arrayList;
    }

}
