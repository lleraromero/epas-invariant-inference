package ar.uba.dc.listitr.java.util;

import java.util.List;
import java.util.RandomAccess;

@SuppressWarnings("rawtypes")
public interface ArrayListInterface extends List, RandomAccess, Cloneable, java.io.Serializable {

}