package ar.uba.dc.listitr;

import randoop.main.Main;

import java.io.IOException;


public class Runner {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        if (args.length != 0)
            throw new Error("Don't pass arguments to ar.uba.dc.listitr.Runner");
        runRandoop();
    }

    private static void runRandoop() throws ClassNotFoundException, IOException {
        System.out.println("Generating Test Cases with Randoop");
        Main.main(new String[]{
                "gentests",
                "--testclass=ar.uba.dc.listitr.java.util.ArrayList$ListItr",
                "--testclass=ar.uba.dc.listitr.java.util.ListItrFactory",
                "--junit-output-dir=src/test/java/",
                "--maxsize=20",
                "--inputlimit=500",
                // "--timelimit=7",
                "--testsperfile=100",
                "--forbid-null=true",
                "--junit-classname=ListItrTest",
                "--junit-package-name=ar.uba.dc.listitr.test",
                "--pretty-print=true",
                "--junit-reflection-allowed=false",
                // "--usethreads=true",
                // "--timeout=5000",
                // "--log=randoop.log"
            });
    }
}
