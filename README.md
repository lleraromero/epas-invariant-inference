# README #

### How do I get set up? ###
* Install [contractor-net] (https://bitbucket.org/lleraromero/contractor-net)
* Install [Daikon] (http://plse.cs.washington.edu/daikon/)
* Read [Wiki] (https://bitbucket.org/lleraromero/epas-invariant-inference/wiki/Home)

### Main Repository Structure: ###

* **EPAs :** Directory of generated EPAs.
 
* **target :** It's the working directory. It contains the classes and test-clases binaries. 

* **src :** Java code directory. This directory contains the ListItr classes that Randoop and Daikon use for generating tests and inferring invariants respectively.

* **ListItr :** C# ListItr solution folder. This directory contains the ListItr project with ListItr code used by contractor-net for generating the EPA.

* **lib :** Necessary libraries for running the experiment.

* Makefile